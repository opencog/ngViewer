ngViewer
================

Visualiser for Opencog - Atomspace
https://github.com/opencog/atomspace

Run
---
```sh
./StartServer
```
Goto http://localhost:8000 in your browser.


